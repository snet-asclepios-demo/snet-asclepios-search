'use strict';

import './index.scss'

// import { uploadData, search, updateData, deleteData, uploadKeyG,
//        computeKey, uploadSSEkeys, asmcrypto_downloadDecryptBuffer, getSSEkeys } from 'expose-loader?exposes=sse!asclepios-sse-client/sse/static/js/sse.js';

import { search, getSSEkeys, asmcrypto_downloadDecryptBuffer } from 'asclepios-sse-client'

let keycloak;

/* search widget */

function createResult(data) {
  let result = document.querySelector('#result-tmpl').cloneNode(true);
  result.removeAttribute('id');
  result.classList.remove('is-hidden');

  let fileName = data.asclepiosPath.substring(data.asclepiosPath.lastIndexOf('/') + 1)
  let title = result.querySelector('.result-title');
  title.textContent = `${data.type}: ${fileName}`;

  let modal = result.querySelector(`.metadata-modal`);

  let metadata = modal.querySelector('.metadata');
  metadata.textContent = JSON.stringify(data, null, 2);

  let metaDataButton = result.querySelector('.metadata-button');
  metaDataButton.addEventListener('click', () => modal.classList.add("is-active"));

  let xnatURL = `/xnat/data/projects/${data.project}/subjects/${data.subject}/experiments/${data.experiment}`
  let xnatButton = result.querySelector('.xnat-button');
  xnatButton.addEventListener('click', () => window.open(xnatURL));

  let closeButton = modal.querySelector('.metadata-close-button');
  closeButton.addEventListener('click', () => modal.classList.remove('is-active'));

  return result;
}

async function handleSearchButton(event) {
  let searchWidget = document.querySelector('#search-widget');
  let panel = searchWidget.querySelector('.panel');
  let msg = panel.querySelector('.message');
  let attr = searchWidget.querySelector('#attr');
  let val = searchWidget.querySelector('#val');
  let userProject = keycloak.tokenParsed.project;
  let subjectsUrl = `/xnat/data/projects/${userProject}/subjects?columns=xnat:subjectData/fields/field%5Bname%3Dkeyid%5D/field,xnat:subjectData/meta/last_modified`

  let response = await fetch(subjectsUrl);
  let obj = await response.json();
  let subjects = obj.ResultSet.Result;

  subjects.sort((a, b) => b['last_modified'].localeCompare(a['last_modified']));

  panel.querySelectorAll('.result').forEach((result) => result.remove());
  msg.classList.remove('is-hidden');

  for (let i in subjects) {
    let subject = subjects[i];
    let keyid = subject['xnat:subjectdata/fields/field[name=keyid]/field'];

    let { verKey, encKey } = await getSSEkeys(
      keyid,
      keycloak.tokenParsed.preferred_username,
      keycloak.token
    );

    let response = await search(
      {'keyword': [`${attr.value}|${val.value}`]},
      verKey, encKey, keyid,
      true, false,
      keycloak.token
    );

    response.objects.forEach((obj) => panel.appendChild(createResult(obj)));
  }

  msg.classList.add('is-hidden');
}

function initSearchWidget() {
  let searchWidget = document.querySelector('#search-widget');
  document.querySelector('.search-form')?.addEventListener('submit', e => {
    e.preventDefault();
    handleSearchButton();
  });
  searchWidget.classList.remove('is-hidden');
}

/* singleton widget */

async function initSingletonWidget() {
  let singletonWidget = document.querySelector('#singleton-widget');
  let content = singletonWidget.querySelector('.content');

  let numberOfSignals = document.querySelector('#edf-header-numberOfSignals')
  let start = document.querySelector('#edf-header-start')
  let end = document.querySelector('#edf-header-end')
  let patientIdentification = document.querySelector('#edf-header-patientIdentification')
  let recordIdentification = document.querySelector('#edf-header-recordIdentification')
  let recordHeaderByteSize = document.querySelector('#edf-header-recordHeaderByteSize')
  let numberOfDataRecords = document.querySelector('#edf-header-numberOfDataRecords')
  let recordDurationTime = document.querySelector('#edf-header-recordDurationTime')
  let recordSize = document.querySelector('#edf-header-recordSize')
  let recordSampleSize = document.querySelector('#edf-header-recordSampleSize')

  let urlParams = new URLSearchParams(window.location.search);

  if (!urlParams.has('asclepiosPath')) return;
  if (!urlParams.has('keyid')) return;

  let { verKey, encKey } = await getSSEkeys(
    urlParams.get('keyid'),
    keycloak.tokenParsed.preferred_username,
    keycloak.token
  )

  let response = await search(
    {'keyword': [`asclepiosPath|${urlParams.get('asclepiosPath')}`]},
    verKey, encKey, urlParams.get('keyid'),
    true, false,
    keycloak.token
  )

  if (!response.objects[0]) return;
  let data = response.objects[0];

  numberOfSignals.textContent = data['numberOfSignals']
  start.textContent = data['start']
  end.textContent = data['end']
  patientIdentification.textContent = data['patientIdentification']
  recordIdentification.textContent = data['recordIdentification']
  recordHeaderByteSize.textContent = data['recordHeaderByteSize']
  numberOfDataRecords.textContent = data['numberOfDataRecords']
  recordDurationTime.textContent = data['recordDurationTime']
  recordSize.textContent = data['recordSize']
  recordSampleSize.textContent = data['recordSampleSize']

  content.textContent = JSON.stringify(data, null, 2);

  singletonWidget.classList.remove('is-hidden');
}

/* download widget */

const downloadFile = (blob, fileName) => {
  const link = document.createElement('a');
  // create a blobURI pointing to our Blob
  link.href = URL.createObjectURL(blob);
  link.download = fileName;
  // some browser needs the anchor to be in the doc
  document.body.append(link);
  link.click();
  link.remove();
  // in case the Blob uses a lot of memory
  // setTimeout(() => URL.revokeObjectURL(link.href), 7000);
};


async function initDownloadWidget() {
  let urlParams = new URLSearchParams(window.location.search);
  let downloadWidget = document.querySelector('#download-widget');


  if (!urlParams.has('asclepiosPath')) return;
  if (!urlParams.has('keyid')) return;

  downloadWidget.classList.remove('is-hidden');

  let { encKey } = await getSSEkeys(
    urlParams.get('keyid'),
    keycloak.tokenParsed.preferred_username,
    keycloak.token
  )

  let buffer = await asmcrypto_downloadDecryptBuffer(urlParams.get('asclepiosPath'), encKey, keycloak.token);

  let asclepiosPath = urlParams.get('asclepiosPath');
  let fileName = asclepiosPath.substring(asclepiosPath.lastIndexOf('/') + 1)

  downloadFile(new Blob([buffer]), fileName)
}

/* routing and set up */

function routeHash() {
  let urlHash = window.location.hash;

  if (urlHash.length == 0) { urlHash = "_index"; }

  if (urlHash.startsWith('#get')) {
    initSingletonWidget();
    return;
  }

  if (urlHash.startsWith('#download')) {
    initDownloadWidget();
    return;
  }

  initSearchWidget();
}



document.addEventListener('DOMContentLoaded', (event) => {
  keycloak = new Keycloak('/keycloak.json');
  // window.keycloak = keycloak
  keycloak.init({onLoad: 'login-required', checkLoginIframe: false}).then(function(authenticated) {
    window.addEventListener("hashchange", routeHash);
    routeHash();
  }).catch(function(e) {
      console.log(e);
  });
});
