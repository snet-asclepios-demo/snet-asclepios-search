#!/bin/sh

sed -i -e "s|ta_url|$TA_URL|" $1/main.js;
sed -i -e "s|sse_url|$SSE_URL|" $1/main.js;
sed -i -e "s|salt_value|$SALT|" $1/main.js;
sed -i -e "s|iv_value|$IV|" $1/main.js;
sed -i -e "s|iter_value|$ITER|" $1/main.js;
sed -i -e "s|ks_value|$KS|" $1/main.js;
sed -i -e "s|ts_value|$TS|" $1/main.js;
sed -i -e "s|mode_value|$MODE|" $1/main.js;
sed -i -e "s|adata_value|$ADATA|" $1/main.js;
sed -i -e "s|adata_len_value|$ADATA_LEN|" $1/main.js;
sed -i -e "s|hash_length_value|$HASH_LEN|" $1/main.js;
sed -i -e "s|chunk_size_value|$CHUNK_SIZE|" $1/main.js;
sed -i -e "s|no_chunks_per_upload_value|$NO_CHUNKS_PER_UPLOAD|" $1/main.js;
sed -i -e "s|salt_ta_value|$SALT_TA|" $1/main.js;
sed -i -e "s|iv_ta_value|$IV_TA|" $1/main.js;
sed -i -e "s|iter_ta_value|$ITER_TA|" $1/main.js;
sed -i -e "s|ks_ta_value|$KS_TA|" $1/main.js;
sed -i -e "s|ts_ta_value|$TS_TA|" $1/main.js;
sed -i -e "s|mode_ta_value|$MODE_TA|" $1/main.js;
sed -i -e "s|adata_ta_value|$ADATA_TA|" $1/main.js;
sed -i -e "s|adata_len_ta_value|$ADATA_LEN_TA|" $1/main.js;
sed -i -e "s|sgx_enable_value|$SGX_ENABLE|" $1/main.js;
sed -i -e "s|cp_abe_url|$CP_ABE_URL|" $1/main.js;
sed -i -e "s|debug_value|$DEBUG|" $1/main.js;
sed -i -e "s|auth_value|$AUTH|" $1/main.js;
sed -i -e "s|small_file_value|$SMALL_FILE|" $1/main.js;


cat << EOF > keycloak.json
{
  "realm": "${KEYCLOAK_REALM}",
  "auth-server-url": "${KEYCLOAK_HOST}/auth/",
  "ssl-required": "external",
  "resource": "${KEYCLOAK_RESOURCE}",
  "public-client": true,
  "confidential-port": 0
}
EOF

sed -i "s,\${KEYCLOAK_HOST},${KEYCLOAK_HOST},g" $1/index.html;
