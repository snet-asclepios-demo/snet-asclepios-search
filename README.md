# snet-asclepios-search

Search for and display data from the ASCLEPIOS symmetric searchable encryption service.

## Usage

* Edit `./bin/env.sh` to match the configuration of your snet deployment:

```command
. ./bin/env.sh
npm run build
./bin/entrypoint.sh
```

* The files in `./asclepios-search` are now ready to be served
